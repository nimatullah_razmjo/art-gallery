@extends('layouts.admin')

@section('content')
<h2>Event Details
    <span>
        <a href="{{ route('event.index')}}" class="btn btn-primary float-right" style="display:inline-block;">Back</a>
      </span>
  </h2>
  <hr>
    <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Title:</h3>
    <p >{{$event->title}}</p>

    <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Location:</h3>
    <p >{{$event->location}}</p>
    <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Date:</h3>
    <p >{{$event->event_date}}</p>
    <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Description:</h3>
    <p style="white-space: pre-line">{{$event->description}}</p>
@endsection