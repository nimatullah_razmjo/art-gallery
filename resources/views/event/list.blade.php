@extends('layouts.admin')

@section('content')
<h2>Event List
  <span>
    <a href="{{ route('event.create')}}" class="btn btn-primary float-right" style="display:inline-block;">Add new Event</a>
  </span>
</h2>


<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="table-responsive">
  <table class="table table-striped table-sm" style="text-align:center;">
    <thead>
      <tr>
        <th>Title</th>
        <th>Location</th>
        <th>Event Date</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
      <tr>
        <td>{{$event->title}}</td>
        <td>{{$event->location}}</td>
        <td>{{$event->event_date}}</td>
        <td>
            <a href="{{ route('event.edit',$event->id)}}" class="btn btn-primary" style="display:inline-block;">Edit</a>
            <a href="{{ route('event.show',$event->id)}}" class="btn btn-primary" style="display:inline-block;">View</a>
            <form action="{{ route('event.destroy', $event->id)}}" method="post" style="display:inline-block;">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
      </tr>

      @endforeach
    </tbody>
  </table>
</div>    
@endsection