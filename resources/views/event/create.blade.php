@extends('layouts.admin')

@section('content')
   
<div class="row" style="margin-top:30px">
    <div class="col-sm-8 offset-sm-2">
       <h2 class="display-5">Add new Event</h2>
       <hr>
     <div>
       @if ($errors->any())
         <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
           </ul>
         </div><br />
       @endif
         <form method="post" action="{{ route('event.store') }}" enctype="multipart/form-data">
             @csrf
             <div class="form-group">    
                 <label for="title">Title:</label>
                 <input type="text" class="form-control" name="title" placeholder="Title"/>
             </div>
   
             <div class="form-group">
                 <label for="location">Location:</label>
                 <input type="text" class="form-control" name="location" placeholder="location"/>
             </div>
   
             <div class="form-group">
                 <label for="email">Date:</label>
                 <input type="date" class="form-control" name="event_date" placeholder="Event Date"/>
             </div>
             <div class="form-group">
              <label for="email">Description:</label>
              <textarea name="description"class="form-control" id="description" cols="30" rows="10"></textarea>
          </div>
          
                               
             <button type="submit" class="btn btn-success float-right">Add Event</button>
         </form>
     </div>
   </div>
   </div>

@endsection