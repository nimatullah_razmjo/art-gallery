@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update an Event</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('event.update', $event->id) }}" >
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="title">Title:</label>
                <input type="text" class="form-control" name="title" value={{ $event->title }} />
            </div>

            <div class="form-group">
                <label for="location">Location:</label>
                <input type="text" class="form-control" name="location" value="{{ $event->location }}" />
            </div>
            <div class="form-group">
                <label for="event_date">Date:</label>
                <input type="date" class="form-control" name="event_date" value={{ $event->event_date }} />
            </div>
            <div class="form-group">
                <label for="email">Description:</label>
                <textarea name="description"class="form-control" id="description" cols="30" rows="10">{{ $event->description }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection