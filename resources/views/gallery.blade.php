@extends('layouts.app')

@section('content')
<div class="container">


      @foreach ($arts as $item)
    <div class="card" style="width: 18rem;display: inline-block; margin-left:10px; margin-bottom: 20px;">
    <img src="/images/gallery/{{$item->photo}}" class="card-img-top" alt="{{$item->title}}">
      <div class="card-body">
      <h5 class="card-title">{{$item->title}}</h5>
      <h6 class="card-subtitle mb-2 text-muted"><span class="bold">By:</span> {{$item->artist->name}}</h6>
        <a href="{{ route('art.show',$item->id)}}" class="btn btn-primary" style="display:inline-block;">View</a>
        
      </div>
    </div>
  @endforeach
</div>
@endsection