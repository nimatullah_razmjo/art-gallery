@extends('layouts.app')

@section('content')
<div class="container">
<div class="row ">
    <div class="col-md-12 grid-margin-md stretch-card d-flex_">
        <div class="card">
          <div class="card-body">
            @foreach($events as $event)
                <div class="border p-3 mb-3">
                <div class="pb-3">
                    <div class="row">
                        <div class="col-sm-4 border-right-lg border-right-md-0">
                        <div class="d-flex justify-content-center align-items-center">
                            <h1 class="mb-0 mr-2 text-primary font-weight-normal">{{explode("-",$event->event_date)[2]}}</h1>
                            <div>
                                <p class="font-weight-bold mb-0 text-dark">{{explode("-",$event->event_date)[1]}}</p>
                                <p class="mb-0">{{explode("-",$event->event_date)[0]}}</p>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-8 pl-3">
                        <h4 class="title">{{$event->title}}</h4>
                        <p class="text-dark font-weight-bold mb-0">{{$event->location}}</p>

                        <p>{{$event->description}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
          </div>
        </div>
    </div>
  </div>
</div>
@endsection