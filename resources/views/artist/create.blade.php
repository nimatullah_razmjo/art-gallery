@extends('layouts.admin')

@section('content')
   
<div class="row" style="margin-top:30px">
    <div class="col-sm-8 offset-sm-2">
       <h2 class="display-5">Add new Artist</h2>
       <hr>
     <div>
       @if ($errors->any())
         <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
           </ul>
         </div><br />
       @endif
         <form method="post" action="{{ route('artist.store') }}" enctype="multipart/form-data">
             @csrf
             <div class="form-group">    
                 <label for="name">Name:</label>
                 <input type="text" class="form-control" name="name" placeholder="Name"/>
             </div>
   
             <div class="form-group">
                 <label for="phone">Phone:</label>
                 <input type="text" class="form-control" name="phone" placeholder="Phone"/>
             </div>
   
             <div class="form-group">
                 <label for="email">Email:</label>
                 <input type="text" class="form-control" name="email" placeholder="Email"/>
             </div>
             <div class="form-group">
                 <label for="gender">Gender:</label>
                 <select name="gender" id="gender" class="form-control" >
                     <option value="">Select an Item</option>
                     <option value="male">Male</option>
                     <option value="female">Female</option>
                 </select>
             </div>
             <div class="form-group">
                <label for="email">Age:</label>
                <input type="number" min="10" max="100" class="form-control" name="age" placeholder="Age"/>
            </div>
             <div class="form-group">
                 <label for="country">Picture:</label>
                 <input type="file" name="pic" id="pic">
             </div>
                               
             <button type="submit" class="btn btn-success float-right">Add Artist</button>
         </form>
     </div>
   </div>
   </div>

@endsection