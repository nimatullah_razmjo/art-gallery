@extends('layouts.admin')

@section('content')
<h2>Artist List
  <span>
    <a href="{{ route('artist.create')}}" class="btn btn-primary float-right" style="display:inline-block;">Add new Artist</a>
  </span>
</h2>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="table-responsive">
  <table class="table table-striped table-sm" style="text-align:center; line-height:100px">
    <thead>
      <tr>
        <th>Picture</th>
        <th>Name</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Phone</th>
        <th>age</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
        @foreach($artists as $artist)
      <tr>
        <td>
          @if ($artist->pic)
        <img src="images/artists/{{$artist->pic}}" class="rounded-circle" width="100" height="100" alt="" src="">
          @endif
          
        </td>
        <td>{{$artist->name}}</td>
        <td>{{$artist->email}}</td>
        <td>{{$artist->gender}}</td>
        <td>{{$artist->phone}}</td>
        <td>{{$artist->age}}</td>
        
        <td>
            
            <a href="{{ route('artist.edit',$artist->id)}}" class="btn btn-primary" style="display:inline-block;">Edit</a>

            <form action="{{ route('artist.destroy', $artist->id)}}" method="post" style="display:inline-block;">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </td>
      </tr>

      @endforeach
    </tbody>
  </table>
</div>    
@endsection