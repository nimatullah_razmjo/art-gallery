@extends('layouts.app')

@section('content')
<div class="container">
    
        <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Title:</h3>
        <p >{{$art->title}}</p>
    
        <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">By:</h3>
        {{-- <p >{{$art->artist->name}}</p> --}}
        <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Photo:</h3>
    <img src="/images/gallery/{{$art->photo}}" alt="{{$art->title}}">
        <h3 style="font-size: 1.3em;color: #047bde;font-weight: 500;">Description:</h3>
        <p style="white-space: pre-line">{{$art->description}}</p>
</div>
@endsection