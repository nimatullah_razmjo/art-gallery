@extends('layouts.admin')

@section('content')
<h2>Art List
  <span>
    <a href="{{ route('art.create')}}" class="btn btn-primary float-right" style="display:inline-block;">Add new Photo</a>
  </span>
</h2>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="table-responsive">
  @foreach ($arts as $item)
    <div class="card" style="width: 18rem;display: inline-block; margin-left:10px; margin-bottom: 20px;">
    <img src="/images/gallery/{{$item->photo}}" class="card-img-top" alt="{{$item->title}}">
      <div class="card-body">
      <h5 class="card-title">{{$item->title}}</h5>
      <h6 class="card-subtitle mb-2 text-muted"><span class="bold">By:</span> {{$item->artist->name}}</h6>
        <a href="{{ route('art.show',$item->id)}}" class="btn btn-primary" style="display:inline-block;">View</a>
        <form action="{{ route('art.destroy', $item->id)}}" method="post" style="display:inline-block;">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger" type="submit">Delete</button>
        </form>
      </div>
    </div>
  @endforeach
</div>    
@endsection