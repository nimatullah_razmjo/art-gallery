@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update an Artist</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('artist.update', $artist->id) }}" enctype="multipart/form-data">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={{ $artist->name }} />
            </div>

            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="number" class="form-control" name="phone" value={{ $artist->phone }} />
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value={{ $artist->email }} />
            </div>
            <div class="form-group">
                <label for="gender">Gender:</label>
                <select name="gender" id="gender" class="form-control" value={{$artist->gender}} >
                    <option value="">Select an Item</option>
                    <option value="male" {{($artist->gender =='male') ? 'selected' : ''}}>Male</option>
                    <option value="female" selected={{($artist->gender =='female') ? 'selected' : ''}}>Female</option>
                </select>
            </div>
            <div class="form-group">
               <label for="email">Age:</label>
            <input type="number" min="10" max="100" class="form-control" name="age" placeholder="Age" value="{{$artist->age}}"/>
           </div>
            <div class="form-group">
                <label for="country">Picture:</label>
                <input type="file" name="pic" id="pic">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection