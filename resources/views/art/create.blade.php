@extends('layouts.admin')

@section('content')
   
<div class="row" style="margin-top:30px">
    <div class="col-sm-8 offset-sm-2">
       <h2 class="display-5">Add new Photo</h2>
       <hr>
     <div>
       @if ($errors->any())
         <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
           </ul>
         </div><br />
       @endif
         <form method="post" action="{{ route('art.store') }}" enctype="multipart/form-data">
             @csrf
             <div class="form-group">    
                 <label for="title">Title:</label>
                 <input type="text" class="form-control" name="title" placeholder="Title"/>
             </div>
   
             <div class="form-group">
                 <label for="category">Category:</label>
                 <select name="category" id="category" class="form-control" >
                  <option value="oil-painting">Oil Painting</option>
                  <option value="Sketches">Sketches</option>
                  <option value="Digital arts">Digital arts</option>
                  <option value="Calligraphy">Calligraphy</option>
                  <option value="Photography">Photography</option>
                  <option value="Handicrafts">Handicrafts</option>
              </select>
             </div>
   
             <div class="form-group">
                 <label for="price">Price: AFN</label>
                 <input type="number" class="form-control" name="price" placeholder="price"/>
             </div>
             <div class="form-group">
                 <label for="artist_id">Artist:</label>
                 <select name="artist_id" id="artist_id" class="form-control" >
                   @if ($artists)
                   
                    @foreach ($artists as $artist)
                      <option value={{$artist->id}}>{{$artist->name}}</option>
                    @endforeach
                   @endif
                 </select>
             </div>
             <div class="form-group">
                 <label for="photo">photo:</label>
                 <input type="file" name="photo" id="photo">
             </div>
             <div class="form-group">
                <label for="email">Description:</label>
                <textarea name="description"class="form-control" id="description" cols="30" rows="10"></textarea>
            </div>
            
                               
             <button type="submit" class="btn btn-success float-right">Add Photo</button>
         </form>
     </div>
   </div>
   </div>

@endsection