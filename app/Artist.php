<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'email',
        'gender',
        'age',
        'pic'       
    ];
    protected $table="artists";

    public function arts()
    {
        return $this->hasMany('App\Art');
    }
}
