<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Artist;

class Art extends Model
{
    protected $fillable = [
        "title",
        "category",
        "price", 
        "description",
        "artist_id",
        "photo"
    ];

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }
}
