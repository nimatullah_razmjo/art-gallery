<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Art;


class GalleryController extends Controller
{

    public function index()
    { 
        $arts = Art::with(['artist:name,id'])->paginate(10);
        return view('gallery',compact('arts'));
    }
}
