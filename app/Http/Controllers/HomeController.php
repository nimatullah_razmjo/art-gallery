<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function events() {
        $events = Event::all();
        return view("website/events",compact('events'));
    }
    
    public function about() {
        return view("website/about");
    }

    public function contact() {
        return view("website/contact");
    }

    
}
