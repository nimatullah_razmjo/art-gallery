<?php

namespace App\Http\Controllers;
use App\Artist;
use App\Art;
use Illuminate\Http\Request;

class ArtController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $arts = Art::with(['artist:name,id'])->paginate(10);
        return view('art.list', compact('arts'));
    }

    public function create() {
        $artists = Artist::all();
        return view('art.create',compact(['artists']));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'category'=>'required',
            'price'=>'required',
            'description'=>'required',
            'artist_id'=>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
         $imagename=null;   
         
         if ($request->hasFile('photo')) {
            $imagePath = $request->file('photo');
            $imageName = time() . '.' . $imagePath ->getClientOriginalExtension();

            $imagePath->move('images/gallery', $imageName );
            $imagename=$imageName;
        };
        $art = new Art([
            'title' => $request->get('title'),
            'category' => $request->get('category'),
            'price' => $request->get('price'),
            'description' => $request->get('description'),
            'artist_id' => $request->get('artist_id'),
            'photo' => $imagename
        ]);
        $art->save();
        return redirect('/art')->with('success', 'New photo saved successfully!');  
    }

    public function show(Art $art)
    {
        $art= $art->load('artist:name,id');
        return view('art.show', compact('art'));
    }   

    public function destroy($id)
    {
        $art = Art::find($id);
        $art->delete();
        return redirect('/art')->with('success', 'art deleted successfully!');
    }
}
