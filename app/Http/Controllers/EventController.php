<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $events = Event::all();
        return view('event.list', compact('events'));
    }

    public function create() {
        return view('event.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'location'=>'required',
            'event_date'=>'required',
            'description'=>'required'
        ]);
        
        $event = new Event([
            'title' => $request->get('title'),
            'location' => $request->get('location'),
            'event_date' => $request->get('event_date'),
            'description' => $request->get('description'),
        ]);
        
        $event->save();
        return redirect('/event')->with('success', 'event saved!');   
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return view('event.edit', compact('event')); 
    }

    public function show($id)
    {
        $event = Event::find($id);
        return view('event.show', compact('event'));
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'title'=>'required',
            'location'=>'required',
            'event_date'=>'required',
            'description'=>'required'
        ]);

        $event = Event::find($id);
        $event->title =  $request->get('title');
        $event->location = $request->get('location');
        $event->event_date = $request->get('event_date');
        $event->description = $request->get('description');
        
        $event->save();

        return redirect('/event')->with('success', 'event updated successfully!');
    }

    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();

        return redirect('/event')->with('success', 'event deleted successfully!');
    }

    public function website() {
        $events = Event::all();
        return view('event.website', compact('events'));
    }
}
