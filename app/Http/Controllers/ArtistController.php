<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;

class ArtistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $artists = Artist::all();
        return view('artist.list', compact('artists'));
    }

    public function create() {
        return view('artist.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'gender'=>'required',
            'pic'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
         $imagename=null;   
         
         if ($request->hasFile('pic')) {
            $imagePath = $request->file('pic');
            $imageName = time() . '.' . $imagePath ->getClientOriginalExtension();

            $imagePath->move('images/artists', $imageName );
            $imagename=$imageName;
        };
        $artist = new Artist([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'gender' => $request->get('gender'),
            'age' => $request->get('age'),
            'pic' => $imagename
        ]);
        // echo $imagename;
        // print_r($artist); exit;
        $artist->save();
        return redirect('/artist')->with('success', 'Artist saved!');   
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $artist = Artist::find($id);
        return view('artist.edit', compact('artist')); 
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'gender'=>'required',
        ]);

        $artist = Artist::find($id);
        $artist->name =  $request->get('name');
        $artist->email = $request->get('email');
        $artist->phone = $request->get('phone');
        $artist->gender = $request->get('gender');
        $artist->age = $request->get('age');
        
        if ($request->hasFile('pic')) {
            $imagePath = $request->file('pic');
            $imageName = time() . '.' . $imagePath ->getClientOriginalExtension();

            $imagePath->move('images/artists', $imageName );
            $artist->pic=$imageName;
        };

        $artist->save();

        return redirect('/artist')->with('success', 'artist updated successfully!');
    }

    public function destroy($id)
    {
        $artist = Artist::find($id);
        $artist->delete();

        return redirect('/artist')->with('success', 'artist deleted successfully!');
    }
}
