<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)->nullable()->default(null);
            $table->string('email', 100)->nullable()->default(null);
            $table->string('phone', 100)->nullable()->default(null);
            $table->enum('gender', ['male', 'female'])->nullable()->default('female');
            $table->integer('age')->unsigned()->nullable()->default(12);
            $table->string('pic', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
