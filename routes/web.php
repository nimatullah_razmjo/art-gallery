<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/gallery', 'GalleryController@index')->name('gallery');
Route::get('/gallery/{id}', 'GalleryController@show')->name('gallery');
Route::get('admin', 'adminPanel@index')->name('gallery');
Route::get('events', 'HomeController@events')->name('event');
Route::get('about-us', 'HomeController@about')->name('event');
Route::get('contact-us', 'HomeController@contact')->name('event');


Route::resource('artist', 'ArtistController');
Route::resource('event', 'EventController');
Route::resource('art', 'ArtController');